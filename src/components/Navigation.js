import React from "react";
import { connect } from 'react-redux';
import { NavLink } from "react-router-dom";
import {clearCompletedTodos, deleteTodo} from "../Redux/action/Action.js";

class Navigation extends React.Component {
    constructor(props){
        super(props);
      }

      clearCompleted = (event) => {
        this.props.clearCompletedTodos();
    
        // const stateCopy = [...this.state.todos]
        //   let results = stateCopy.filter(x  => x.completed===false)
        // this.setState({todos: results})
        // console.log(stateCopy)
        console.log('working');
      }

    render(){

        return(
        <>
        <footer className="footer">

          {/* <!-- This should be `0 items left` by default --> */}
          <span className="todo-count">

          <strong>0</strong> item(s) left
          </span>
            
            <ul className="filters">
                <li>
                    <NavLink exact to="/" activeClassName="selected">All</NavLink>
  
                </li>
                <li>
                    <NavLink to="/active" activeClassName="selected">Active</NavLink>
                </li>
                <li>
                    <NavLink to="/completed" activeClassName="selected">Completed</NavLink>

                    <button 
                    onClick={this.clearCompleted} 
                    className="clear-completed">Clear completed
                    </button>
                </li>
            </ul>  
            </footer>
        </>
        )
    }
}

const mapDispatchToProps = {
    clearCompletedTodos, deleteTodo
  }

export default connect(null, mapDispatchToProps)(Navigation);