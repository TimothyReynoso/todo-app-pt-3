import React, {Component} from "react";
//step 3
import {connect} from 'react-redux';
import TodoItem from "../components/TodoItem.js";
//import reducer from "../Redux/reducer/Reducer.js";

//step 2
//importing action creators from file
//import {addTodo} from '../Redux/action/Action'
import {toggleTodo} from '../Redux/action/Action'
//import {clearCompletedTodos} from '../Redux/action/Action'
import {deleteTodo} from '../Redux/action/Action'

//import {text} from '../Redux/action/Action'
//import {todoId} from '../Redux/action/Action'



class TodoList extends Component {
  constructor(props){
    super(props);
  }
    
handleDelete = (event, todoId) => {
  this.props.deleteTodo(todoId)
  // const deleteAccount = this.state.todos.filter(
  //   todo => todo.id !== todoId
  // )
  // this.setState({ todos: deleteAccount })
}

isBoxChecked = (event) => {
  this.props.toggleTodo(event);
   
   // const stateCopy = [...this.state.todos] //copying our state
     
   // stateCopy.map ( arrayProperty => {if(event.target.id == arrayProperty.id){
   //   arrayProperty.completed = !arrayProperty.completed
   // }})

   // this.setState({todos: stateCopy})
   
  console.log(event)
 }

    
    render() {
      console.log(this.props.todos)
      return (
        <section className="main">
          <ul className="todo-list">
            {this.props.todos.map(todo => (
              <TodoItem 
              key={todo.id}
              id={todo.id} 
              handleDelete={ () => this.handleDelete(todo.id)} 
              isBoxChecked={ () => this.isBoxChecked(todo.id)} 
              title={todo.title} 
              completed={todo.completed} 
              />
            ))}
          </ul>
        </section>
      );
    }
  }

//step 1 
// const mapStateToProps = state => {
//   return {
//     todos: state.todos
//   }
// }

//step 2 

// const mapDispatchToProps = dispatch => {
//   return{//return/dispatch our action creators
//     addTodod: () => dispatch(addTodo(text)),
  
//     toggleTodo: () => dispatch(toggleTodo(todoId)),
  
//     clearCompletedTodos: () => dispatch(clearCompletedTodos()),
  
//     deleteTodo: () => dispatch(deleteTodo(todoId))
//   }
// } 

const mapDispatchToProps = {
  deleteTodo,toggleTodo
}

//step 3
//connecting the MSTP and MDTP together with 
// the react component. 

// before {  export default TodoList;  }
//step 3 after 
export default connect(null, mapDispatchToProps)(TodoList);