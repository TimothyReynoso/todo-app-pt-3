import React from 'react';
import { connect } from 'react-redux';
import {deleteTodo} from '../Redux/action/Action.js'

class TodoItem extends React.Component {
    //function that checks boxs complete
    constructor(props){
      super(props);
    }

    handleDelete = (event, todoId) => {
      this.props.deleteTodo(todoId)
      // const deleteAccount = this.state.todos.filter(
      //   todo => todo.id !== todoId
      // )
      // this.setState({ todos: deleteAccount })
      
    }

      render() {
        //console.log(this.props.id)
        return (
          <li  className={this.props.completed ? "completed" : ""}>
            <div className="view">
              <input  
              onChange={event=> this.props.isBoxChecked(event, this.props.id)} 
              className="toggle" 
              type="checkbox"
              checked={this.props.completed}
              id={this.props.id}
              />
              <label >{this.props.title}</label>
              <button  
              onClick={this.handleDelete} 
              className="destroy" 
              />
            </div>
          </li>
        );
      }
    }

const mapDispatchToProps = {
  deleteTodo
}

export default connect(null, mapDispatchToProps)(TodoItem);