import React, { Component } from "react";
import {connect} from 'react-redux';
//import todosList from "./todos.json";
import {Route , Switch} from "react-router-dom";
//import TodoItem from "../src/components/TodoItem.js"
import TodoList from "../src/components/TodoList.js"
import Navigation from "../src/components/Navigation.js"

//import store from './Redux/store.js'
import { addTodo, deleteTodo } from "./Redux";
import { toggleTodo, clearCompletedTodos } from "./Redux"


class App extends Component {
  constructor(props){
    super(props);
  }
 
  // state = {
  //   todos: todosList
  // };

  moveUserInputToList = (event) => {
   
    //TODO: Move input text to the item list  ... const {todos} = this.state
    
    if ( event.key === 'Enter'){
       this.props.addTodo(event.target.value)
    }

    //   const stateCopy = [...this.state.todos] //copying our todoslist, the brackets make this copy an array

    //   const newtodo = { // someething that we want to add to the todos (in this case an object)
    //     "userId": 1,
    //     "id": Math.floor(Math.random() * 1000),
    //     "title": event.target.value,
    //     "completed": false
    //   }
    //   stateCopy.push(newtodo) // moving the (something) in this case an object into the stateCopy array
      
    //  // this.setState({todos: stateCopy}) //useing setState to give todos a new item.. an array of objects.
    
    // */
    //}
  }



  isBoxChecked = (event) => {
   this.props.toggleTodo(event.target.id);
    
    // const stateCopy = [...this.state.todos] //copying our state
      
    // stateCopy.map ( arrayProperty => {if(event.target.id == arrayProperty.id){
    //   arrayProperty.completed = !arrayProperty.completed
    // }})
 
    // this.setState({todos: stateCopy})
    
   console.log(event)
  }

  clearCompleted = (event) => {
    //this.props.clearCompletedTodos();

    // const stateCopy = [...this.state.todos]
    //   let results = stateCopy.filter(x  => x.completed===false)
    // this.setState({todos: results})
    // console.log(stateCopy)
    console.log('working');
  }


  handleDelete = (event, todoId) => {
    this.props.deleteTodo(todoId)
    // const deleteAccount = this.state.todos.filter(
    //   todo => todo.id !== todoId
    // )
    // this.setState({ todos: deleteAccount })
  }



  render() {
    console.log(this.props)
    return (
     //<Provider store={store}>
        <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input 
          onKeyDown={this.moveUserInputToList} 
          className="new-todo" 
          placeholder="What needs to be done?" 
          autofocus 
          />
        </header>
        <Switch>
                <Route exact path="/"
                    render={() => (
                    <TodoList  
                    // handleDelete={this.handleDelete} 
                    // isBoxChecked={this.isBoxChecked} 
                    todos={this.props.todos}
                    />)}
                />
                <Route path="/active"
                    render={() => (
                    <TodoList  
                    // handleDelete={this.handleDelete} 
                    // isBoxChecked={this.isBoxChecked} 
                    todos={this.props.todos.filter( todo => todo.completed === false)} />)}
                />
                <Route exact path="/completed" 
                    render={() => (
                    <TodoList  
                    // handleDelete={this.handleDelete} 
                    // isBoxChecked={this.isBoxChecked} 
                    todos={this.props.todos.filter( todo => todo.completed === true)} />)}
                                
                />
                
            </Switch>
          
          {/* <Navigation todosList={this.state.todos}/> */}
          <Navigation 
          handleClearConmpleted={this.clearCompleted}/>



        </section>

        
        
     //</Provider>
    );
  }
}

const mapStateToProps = state => {
  return {todos: state.todos}
}

const mapDispatchToProps = {
  addTodo,
  toggleTodo,
  clearCompletedTodos,
  deleteTodo


}

export default connect(mapStateToProps, mapDispatchToProps)(App);

