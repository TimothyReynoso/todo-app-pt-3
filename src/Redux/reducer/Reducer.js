import todosList from  '../../todos.json'
import {TOGGLE_TODO, ADD_TODO, DELETE_TODO, CLEAR_COMPLETED_TODOS} from '../action/Action.js'
//import TodoItem from '../../components/TodoItem';

const initialState = {
    todos: todosList
}

const reducer = (state = initialState, action) => {

    switch(action.type){

        case TOGGLE_TODO:
         console.log('inside reducer toggle todos');
         console.log(action);
        return Object.assign({},state, {
            
            ...state, 
            todos: state.todos.map(todoItem => {if(action.payload === todoItem.id){
                return Object.assign({},todoItem,{ completed: !todoItem.completed })
                 
            }
                return todoItem
        })
            // fruit: 1+1,
            // meat: listOfMeat.map(meatItem)
        })
        
        // Object.assign({},state,{
        //     const newTodos = state.todos.slice();
        //     newTodos.map()

        case ADD_TODO:
            //updating state.... taking object/state/how to update
        return Object.assign({},state, {
            todos: [
                ...state.todos, 
                {
                    userId: action.userId,
                    id: action.id,
                    title: action.title,
                    completed: !action.completed
                }  
            ]
        })

        // { todos:[

        
        //     ...state.todos,
        //     { 
        //         userId: action.userId,
        //         id: action.id,
        //         title: action.title,
        //         completed: action.completed
        //     }
        //     ]
        // }
        
        
        case DELETE_TODO:
        return Object.assign({}, state, {
            todos: state.todos.filter(
                todo => {
                    if (todo.id === action.id){
                        return action.id;
                    } 
                    return action.id;
                }
              )
        })
        /*
        handleDelete = (event, todoId) => {
            const deleteAccount = state.filter(
              todo => todo.id !== todoId
            )
            this.setState({ todos: deleteAccount })
          }
          */
        case CLEAR_COMPLETED_TODOS:
        return  Object.assign({},state, {
            todos: state.todos.filter(
                todo => todo.completed === false
                
            )
        })
        
        
        // clearCompleted = (event) => {
        //     const stateCopy = [...state]
        //       let results = stateCopy.filter(x  => x.completed===false)
            
        //   }
        default:
        return state;
    }
}

export default reducer;