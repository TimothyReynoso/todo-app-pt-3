export const ADD_TODO = 'ADD_TODO';
export const TOGGLE_TODO = 'TOGGLE_TODO';
export const CLEAR_COMPLETED_TODOS = 'CLEAR_COMPLETED_TODOS';
export const DELETE_TODO = 'DELETE_TODO';

//what happens to the reducer
//action creators
export function addTodo(text){
    return {type: ADD_TODO, 
        //data that needs to be put into state
        userId: 1,
        id: Math.floor(Math.random() * 1000),
        title: text,
        completed: false}
}

export function toggleTodo(propertyId){
    return {type: TOGGLE_TODO, 
        //data that needs to be put into state
        payload: propertyId}
}

export function clearCompletedTodos(){
    console.log('action creator');
    return {type: CLEAR_COMPLETED_TODOS,}
}

export function deleteTodo(todoId){
    return {type: DELETE_TODO, 
        id: todoId}
}