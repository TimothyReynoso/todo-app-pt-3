//index.js is just a file to export all action creators

//step 2
export {addTodo} from '../Redux/action/Action'
export {toggleTodo} from '../Redux/action/Action'
export {clearCompletedTodos} from '../Redux/action/Action'
export {deleteTodo} from '../Redux/action/Action'
